module.exports = {
    name: `plugin-hello-world`,
    factory: require => {
      const {Command} = require(`clipanion`);
  
      class Greet extends Command {
        async execute() {

          const { exec } = require('child_process');
          if(this.user) {
            const exec = require('child_process').exec;
            const shellScript = exec(`sh sys.sh ${this.user}👋 /`);
            shellScript.stdout.on('data', (data)=>{
                console.log(data); 
            });
            shellScript.stderr.on('data', (data)=>{
                console.error(data);
            });
          }else {
            console.log('No name passed')
            console.log('More info on how to use this command:')
            console.log('  yarn hello --help')
          }
          
    
        }
      }
  
      Greet.addOption(`user`, Command.String(`--u`));
  
      Greet.addPath(`hello`);
  
      Greet.usage = Command.Usage({
        description: `hello world!`,
        details: `
          This command will print a nice greeting message.
        `,
        examples: [[
          `Say hello to an Jack`,
          `yarn hello --u Jack`,
        ]],
      });
  
      return {
        commands: [
          Greet,
        ],
        hooks: {
          afterAllInstalled
        }
      };
    },
  };

const afterAllInstalled = project => {
  console.log("Everything is installed 🎉");
  };